const fs = require("fs");
const path = require("path");
const jsonPath = path.join(__dirname, "jsonFiles", "boards.json");


function boardInformation(boardID, callback) {

  setTimeout(() => {

    fs.readFile(jsonPath, "utf8", (err, data) => {

      if (err) {
        callback(err);

      } else {
        data = JSON.parse(data);

        const boardIDInfo = data.find((item) => {
          return item.id === boardID;
        });
        
        // console.log(boardIDInfo);
        callback(null, boardIDInfo);
      }
    });
  }, 2 * 1000);
}

module.exports = boardInformation;
