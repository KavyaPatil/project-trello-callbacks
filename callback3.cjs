const fs = require("fs");
const path = require("path");
const jsonPath = path.join(__dirname, "jsonFiles", "cards.json");

function gettingCardsFromTheGivenListID(listID, callback) {

  setTimeout(() => {

    fs.readFile(jsonPath, "utf8", (err, data) => {

      if (err) {
        callback(err);

      } else {
        const dataObject = JSON.parse(data);

        const objectKey = Object.keys(dataObject).find((key) => {
          return key === listID;

        });

        const resultData = dataObject[objectKey];
        
        callback(null, resultData);
      }
    });
  }, 2 * 1000);
}

module.exports = gettingCardsFromTheGivenListID;
