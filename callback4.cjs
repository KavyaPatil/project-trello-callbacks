const callback1 = require("./callback1.cjs");
const callback2 = require("./callback2.cjs");
const callback3 = require("./callback3.cjs");

function callbacksFunction(boardID) {

  setTimeout(() => {

    callback1(boardID, (err, callback1Data) => {
      if (err) {
        console.log(err);
      } else {
        console.log(callback1Data);

        callback2(boardID, (err, callback2Data) => {
          if (err) {
            console.log(err);
          } else {
            console.log(callback2Data);

            const input = callback2Data.find((item) => {
              return item.name === "Mind";
            });
            console.log(input);

            callback3(input.id, (err, callback3Data) => {
              if (err) {
                console.log(err);
              } else {
                console.log(item.name,callback3Data);
              }
            });
          }
        });
      }
    });
  }, 2 * 1000);
}

module.exports = callbacksFunction;
