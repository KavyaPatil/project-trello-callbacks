const fs = require("fs");
const path = require("path");
const jsonPath = path.join(__dirname, "jsonFiles", "lists.json");

function gettingBoardLists(boardID, callback) {

  setTimeout(() => {

    fs.readFile(jsonPath, "utf8", (err, data) => {

      if (err) {
        callback(err);

      } else {
        const dataObject = JSON.parse(data);

        const objectKey = Object.keys(dataObject).find((key) => {
          return key === boardID;

        });

        const resultData = dataObject[objectKey];

        callback(null, resultData);
      }
    });
  }, 2 * 1000);
}

module.exports = gettingBoardLists;
